<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PerfilPostulanteController;
use App\Http\Controllers\PerfilEmpleadorController;
use App\Http\Controllers\PublicarOfertaController;
use App\Http\Controllers\ModificarOfertaController;

Route::get('/', [HomeController::class, 'index']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/perfil-postulante', [PerfilPostulanteController::class, 'index']);
Route::get('/perfil-empleador', [PerfilEmpleadorController::class, 'index']);
Route::get('/publicar-oferta', [PublicarOfertaController::class, 'index']);
Route::get('/modificar-oferta', [ModificarOfertaController::class, 'index']);
