<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\DatosPersonale;
use App\Models\Empresa;

class RegisterController extends Controller
{
    // Muestra el formulario de registro
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {

        if ($request->has('tipo_usuario') && $request->tipo_usuario == 'empleador') {
            // Validaciones específicas para empleador
            $request->validate([
                'ruc' => 'required|string|max:255',
                'razon_social' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:Usuarios,Email',
                'email_confirmation' => 'required|same:email',
                'password' => 'required|string|min:6|confirmed',
            ]);
            // Crear registro en Empresa
            $empresa = Empresa::create([
                'Ruc' => $request->ruc,
                'RazonSocial' => $request->razon_social,
            ]);
            $user = Usuario::create([
                'Username' => $request->ruc,
                'Email' => $request->email,
                'Password' => bcrypt($request->password),
                'idEmpresas' => $empresa->IdEmpresa,
                'FechaAlta' => now(),
                'TipUsuario' => 'EMPLEADOR',
            ]);
        } else {
            $request->validate([
                // Valida los campos necesarios para DatosPersonale y Usuario
                'nombre' => 'required|string|max:255',
                'apellido' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:Usuarios,Email',
                'email_confirmation' => 'required|same:email',
                'password' => 'required|string|min:6|confirmed',
            ]);
            $datosPersonale = DatosPersonale::create([
                'Nombres' => $request->nombre,
                'Apellidos' => $request->apellido,
                'Email' => $request->email,
            ]);
            $user = Usuario::create([
                'Username' => $request->nombre,
                'Email' => $request->email,
                'Password' => bcrypt($request->password),
                'IdDatosPersonales' => $datosPersonale->IdDatosPersonales,
                'FechaAlta' => now(),
                'TipUsuario' => 'POSTULANTE',
            ]);
        }
        // Loguear al usuario y redirigir
        auth()->login($user);

        return redirect()->route('home'); // Ajusta la ruta según sea necesario
    }
}
