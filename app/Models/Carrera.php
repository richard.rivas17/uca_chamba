<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Carrera
 * 
 * @property int $IdCarrera
 * @property string $Carrera
 * @property int $IdTipoGrado
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Carrera extends Model
{
	protected $table = 'Carreras';
	protected $primaryKey = 'IdCarrera';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdCarrera' => 'int',
		'IdTipoGrado' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Carrera',
		'IdTipoGrado',
		'FechaAlta'
	];
}
