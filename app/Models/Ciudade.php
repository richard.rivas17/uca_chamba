<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ciudade
 * 
 * @property int $IdCiudad
 * @property string|null $Nombre
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Ciudade extends Model
{
	protected $table = 'Ciudades';
	protected $primaryKey = 'IdCiudad';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdCiudad' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Nombre',
		'FechaAlta'
	];
}
