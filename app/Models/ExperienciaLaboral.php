<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ExperienciaLaboral
 * 
 * @property int $IdExperienciaLaboral
 * @property int|null $TieneExperiencia
 *
 * @package App\Models
 */
class ExperienciaLaboral extends Model
{
	protected $table = 'ExperienciaLaboral';
	protected $primaryKey = 'IdExperienciaLaboral';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdExperienciaLaboral' => 'int',
		'TieneExperiencia' => 'int'
	];

	protected $fillable = [
		'TieneExperiencia'
	];
}
