<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Paise
 * 
 * @property int $IdPais
 * @property string $Pais
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Paise extends Model
{
	protected $table = 'Paises';
	protected $primaryKey = 'IdPais';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdPais' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Pais',
		'FechaAlta'
	];
}
