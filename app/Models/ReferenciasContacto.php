<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferenciasContacto
 * 
 * @property int $Id
 * @property int $IdTipoReferencia
 * @property int $IdPerfil
 * @property int|null $IdExperienciaProfesional
 * @property string|null $NombreContacto
 * @property string $Telefono
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class ReferenciasContacto extends Model
{
	protected $table = 'ReferenciasContactos';
	protected $primaryKey = 'Id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'Id' => 'int',
		'IdTipoReferencia' => 'int',
		'IdPerfil' => 'int',
		'IdExperienciaProfesional' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'IdTipoReferencia',
		'IdPerfil',
		'IdExperienciaProfesional',
		'NombreContacto',
		'Telefono',
		'FechaAlta'
	];
}
