<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Idioma
 * 
 * @property int $IdIdioma
 * @property string|null $IdiomaNombre
 *
 * @package App\Models
 */
class Idioma extends Model
{
	protected $table = 'Idioma';
	protected $primaryKey = 'IdIdioma';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdIdioma' => 'int'
	];

	protected $fillable = [
		'IdiomaNombre'
	];
}
