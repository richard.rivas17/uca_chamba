<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RubroDeLaEmpresa
 * 
 * @property int $IdRubro
 * @property string|null $Rubro
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class RubroDeLaEmpresa extends Model
{
	protected $table = 'RubroDeLaEmpresa';
	protected $primaryKey = 'IdRubro';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdRubro' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Rubro',
		'FechaAlta'
	];
}
