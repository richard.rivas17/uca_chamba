<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Perfile
 * 
 * @property int $IdPerfil
 * @property int|null $PerfilCompleto
 * @property string|null $UserIdAlumno
 * @property int|null $UserId
 * @property int|null $IdFormacionAcademica
 * @property int|null $IdExperienciaLaboral
 * @property int|null $IdCurriculum
 * @property int|null $IdDatosPersonales
 * @property int|null $TerminosYCondiciones
 * @property Carbon $FechaAlta
 * @property int|null $IdTipoPerfil
 * @property string|null $OAuthUserId
 *
 * @package App\Models
 */
class Perfile extends Model
{
	protected $table = 'Perfiles';
	protected $primaryKey = 'IdPerfil';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdPerfil' => 'int',
		'PerfilCompleto' => 'int',
		'UserId' => 'int',
		'IdFormacionAcademica' => 'int',
		'IdExperienciaLaboral' => 'int',
		'IdCurriculum' => 'int',
		'IdDatosPersonales' => 'int',
		'TerminosYCondiciones' => 'int',
		'FechaAlta' => 'datetime',
		'IdTipoPerfil' => 'int'
	];

	protected $fillable = [
		'PerfilCompleto',
		'UserIdAlumno',
		'UserId',
		'IdFormacionAcademica',
		'IdExperienciaLaboral',
		'IdCurriculum',
		'IdDatosPersonales',
		'TerminosYCondiciones',
		'FechaAlta',
		'IdTipoPerfil',
		'OAuthUserId'
	];
}
