<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InteresesYPreferencia
 * 
 * @property int $IdInteresesYPreferencias
 * @property int|null $IdAreaLaboral
 * @property int|null $IdPerfil
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class InteresesYPreferencia extends Model
{
	protected $table = 'InteresesYPreferencias';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdInteresesYPreferencias' => 'int',
		'IdAreaLaboral' => 'int',
		'IdPerfil' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'IdInteresesYPreferencias',
		'IdAreaLaboral',
		'IdPerfil',
		'FechaAlta'
	];
}
