<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OfertaIdiomaRequerido
 * 
 * @property int $IdOfertaIdiomaRequerido
 * @property int|null $IdIdioma
 * @property int|null $IdNivelesDeConocimiento
 * @property int|null $IdPropiedad
 * @property int|null $IdRequerimiento
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class OfertaIdiomaRequerido extends Model
{
	protected $table = 'OfertaIdiomaRequerido';
	protected $primaryKey = 'IdOfertaIdiomaRequerido';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdOfertaIdiomaRequerido' => 'int',
		'IdIdioma' => 'int',
		'IdNivelesDeConocimiento' => 'int',
		'IdPropiedad' => 'int',
		'IdRequerimiento' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'IdIdioma',
		'IdNivelesDeConocimiento',
		'IdPropiedad',
		'IdRequerimiento',
		'FechaAlta'
	];
}
