<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cargo
 * 
 * @property int $IdCargo
 * @property string $Cargo
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Cargo extends Model
{
	protected $table = 'Cargos';
	protected $primaryKey = 'IdCargo';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdCargo' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Cargo',
		'FechaAlta'
	];
}
