<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Archivo
 * 
 * @property int $IdArchivo
 * @property string|null $Nombre
 * @property string|null $Extension
 * @property float|null $Tamanio
 * @property string|null $Ubicacion
 * @property string|null $MimeType
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Archivo extends Model
{
	protected $table = 'Archivos';
	protected $primaryKey = 'IdArchivo';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdArchivo' => 'int',
		'Tamanio' => 'float',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Nombre',
		'Extension',
		'Tamanio',
		'Ubicacion',
		'MimeType',
		'FechaAlta'
	];
}
