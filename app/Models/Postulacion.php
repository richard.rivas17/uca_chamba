<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Postulacion
 * 
 * @property int $IdPostulaciones
 * @property int|null $IdOferta
 * @property int|null $IdPerfil
 * @property Carbon|null $FechaPostulacion
 *
 * @package App\Models
 */
class Postulacion extends Model
{
	protected $table = 'Postulacion';
	protected $primaryKey = 'IdPostulaciones';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdPostulaciones' => 'int',
		'IdOferta' => 'int',
		'IdPerfil' => 'int',
		'FechaPostulacion' => 'datetime'
	];

	protected $fillable = [
		'IdOferta',
		'IdPerfil',
		'FechaPostulacion'
	];
}
