<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Curriculum
 * 
 * @property int $IdCurriculum
 * @property string|null $Titulo
 * @property string|null $TipoDocumento
 * @property string|null $Url
 * @property Carbon $FechaAlta
 * @property float|null $Size
 *
 * @package App\Models
 */
class Curriculum extends Model
{
	protected $table = 'Curriculum';
	protected $primaryKey = 'IdCurriculum';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdCurriculum' => 'int',
		'FechaAlta' => 'datetime',
		'Size' => 'float'
	];

	protected $fillable = [
		'Titulo',
		'TipoDocumento',
		'Url',
		'FechaAlta',
		'Size'
	];
}
