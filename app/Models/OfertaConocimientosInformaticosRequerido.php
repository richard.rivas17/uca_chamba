<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OfertaConocimientosInformaticosRequerido
 * 
 * @property int $IdOfertaConocimientosInformaticosRequerido
 * @property int|null $IdConocimientoInformatico
 * @property int|null $IdNivelesDeConocimiento
 * @property int|null $IdPropiedad
 * @property int|null $IdRequerimiento
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class OfertaConocimientosInformaticosRequerido extends Model
{
	protected $table = 'OfertaConocimientosInformaticosRequerido';
	protected $primaryKey = 'IdOfertaConocimientosInformaticosRequerido';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdOfertaConocimientosInformaticosRequerido' => 'int',
		'IdConocimientoInformatico' => 'int',
		'IdNivelesDeConocimiento' => 'int',
		'IdPropiedad' => 'int',
		'IdRequerimiento' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'IdConocimientoInformatico',
		'IdNivelesDeConocimiento',
		'IdPropiedad',
		'IdRequerimiento',
		'FechaAlta'
	];
}
