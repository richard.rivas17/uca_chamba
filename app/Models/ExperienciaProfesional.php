<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ExperienciaProfesional
 * 
 * @property int $IdExperienciaProfesional
 * @property int|null $IdExperienciaLaboral
 * @property string|null $Compania
 * @property int|null $IdCargoOcupado
 * @property string|null $BreveDescripcion
 * @property Carbon|null $FechaInicio
 * @property Carbon|null $FechaFin
 * @property int|null $IdDepartamento
 *
 * @package App\Models
 */
class ExperienciaProfesional extends Model
{
	protected $table = 'ExperienciaProfesional';
	protected $primaryKey = 'IdExperienciaProfesional';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdExperienciaProfesional' => 'int',
		'IdExperienciaLaboral' => 'int',
		'IdCargoOcupado' => 'int',
		'FechaInicio' => 'datetime',
		'FechaFin' => 'datetime',
		'IdDepartamento' => 'int'
	];

	protected $fillable = [
		'IdExperienciaLaboral',
		'Compania',
		'IdCargoOcupado',
		'BreveDescripcion',
		'FechaInicio',
		'FechaFin',
		'IdDepartamento'
	];
}
