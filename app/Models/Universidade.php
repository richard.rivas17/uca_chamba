<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Universidade
 * 
 * @property int $IdUniversidad
 * @property string $Universidad
 * @property int $IdPaisOrigen
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Universidade extends Model
{
	protected $table = 'Universidades';
	protected $primaryKey = 'IdUniversidad';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdUniversidad' => 'int',
		'IdPaisOrigen' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Universidad',
		'IdPaisOrigen',
		'FechaAlta'
	];
}
