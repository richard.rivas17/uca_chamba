<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoReferencium
 * 
 * @property int $IdTipoReferencia
 * @property string $TipoRef
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class TipoReferencium extends Model
{
	protected $table = 'TipoReferencia';
	protected $primaryKey = 'IdTipoReferencia';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdTipoReferencia' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'TipoRef',
		'FechaAlta'
	];
}
