<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

/**
 * Class Usuario
 * 
 * @property int $Id
 * @property string $Username
 * @property string $Email
 * @property string $Password
 * @property string|null $TipUsuario
 * @property Carbon $FechaAlta
 * @property int $IdDatosPersonales
 * @property inet $idEmpresas
 *
 * @package App\Models
 */
class Usuario extends Model implements AuthenticatableContract
{
	use Authenticatable;
	protected $table = 'Usuarios';
	protected $primaryKey = 'Id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'Id' => 'int',
		'FechaAlta' => 'datetime',
		'IdDatosPersonales' => 'int',
		'idEmpresas' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'Username',
		'Email',
		'password',
		'TipUsuario',
		'FechaAlta',
		'IdDatosPersonales',
		'idEmpresas'
	];
}
