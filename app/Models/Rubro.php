<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rubro
 * 
 * @property int $IdDepartamento
 * @property string $Departamento
 *
 * @package App\Models
 */
class Rubro extends Model
{
	protected $table = 'Rubros';
	protected $primaryKey = 'IdDepartamento';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdDepartamento' => 'int'
	];

	protected $fillable = [
		'Departamento'
	];
}
