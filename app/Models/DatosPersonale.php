<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DatosPersonale
 * 
 * @property int $IdDatosPersonales
 * @property string $Nombres
 * @property string $Apellidos
 * @property string|null $Email
 * @property string|null $Telefono
 * @property string|null $tipoDocumento
 * @property string|null $NroDocumento
 * @property int|null $Edad
 * @property string|null $EstadoCivil
 * @property string|null $Genero
 * @property string|null $Nacionalidad
 * @property Carbon|null $FechaAlta
 * @property int|null $IdCiudad
 * @property Carbon|null $FechaNacimiento
 *
 * @package App\Models
 */
class DatosPersonale extends Model
{
	protected $table = 'DatosPersonales';
	protected $primaryKey = 'IdDatosPersonales';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdDatosPersonales' => 'int',
		'Edad' => 'int',
		'FechaAlta' => 'datetime',
		'IdCiudad' => 'int',
		'FechaNacimiento' => 'datetime'
	];

	protected $fillable = [
		'Nombres',
		'Apellidos',
		'Email',
		'Telefono',
		'tipoDocumento',
		'NroDocumento',
		'Edad',
		'EstadoCivil',
		'Genero',
		'Nacionalidad',
		'FechaAlta',
		'IdCiudad',
		'FechaNacimiento'
	];
}
