<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ModalidadTrabajo
 * 
 * @property int $IdModalidad
 * @property string|null $Nombre
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class ModalidadTrabajo extends Model
{
	protected $table = 'ModalidadTrabajo';
	protected $primaryKey = 'IdModalidad';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdModalidad' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Nombre',
		'FechaAlta'
	];
}
