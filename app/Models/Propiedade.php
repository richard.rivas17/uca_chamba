<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Propiedade
 * 
 * @property int $IdPropiedad
 * @property string $Propiedad
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Propiedade extends Model
{
	protected $table = 'Propiedades';
	protected $primaryKey = 'IdPropiedad';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdPropiedad' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Propiedad',
		'FechaAlta'
	];
}
