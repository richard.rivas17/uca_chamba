<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoPerfil
 * 
 * @property int $IdTipoPerfil
 * @property string $TipoPerfil
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class TipoPerfil extends Model
{
	protected $table = 'TipoPerfil';
	protected $primaryKey = 'IdTipoPerfil';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdTipoPerfil' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'TipoPerfil',
		'FechaAlta'
	];
}
