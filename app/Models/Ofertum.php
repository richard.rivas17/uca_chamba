<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ofertum
 * 
 * @property int $IdOfertas
 * @property string|null $Estado
 * @property Carbon|null $CreadoEn
 * @property string|null $Titulo
 * @property int|null $IdModalidad
 * @property int|null $IdCiudad
 * @property int|null $Salario
 * @property string|null $Beneficios
 * @property int|null $SocializarLogoCliente
 * @property string|null $IdUserMod
 * @property Carbon|null $UltimaModificacion
 * @property int|null $Visible
 * @property int|null $IdEmpresa
 * @property string|null $Descripcion
 * @property int|null $IdRequerimiento
 * @property int|null $IdRubro
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Ofertum extends Model
{
	protected $table = 'Oferta';
	protected $primaryKey = 'IdOfertas';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdOfertas' => 'int',
		'CreadoEn' => 'datetime',
		'IdModalidad' => 'int',
		'IdCiudad' => 'int',
		'Salario' => 'int',
		'SocializarLogoCliente' => 'int',
		'UltimaModificacion' => 'datetime',
		'Visible' => 'int',
		'IdEmpresa' => 'int',
		'IdRequerimiento' => 'int',
		'IdRubro' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Estado',
		'CreadoEn',
		'Titulo',
		'IdModalidad',
		'IdCiudad',
		'Salario',
		'Beneficios',
		'SocializarLogoCliente',
		'IdUserMod',
		'UltimaModificacion',
		'Visible',
		'IdEmpresa',
		'Descripcion',
		'IdRequerimiento',
		'IdRubro',
		'FechaAlta'
	];
}
