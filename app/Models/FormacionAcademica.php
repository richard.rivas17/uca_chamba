<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FormacionAcademica
 * 
 * @property int $IdFormacionAcademica
 * @property int|null $IdNivelSecundario
 * @property int|null $IdNivelUniversitario
 *
 * @package App\Models
 */
class FormacionAcademica extends Model
{
	protected $table = 'FormacionAcademica';
	protected $primaryKey = 'IdFormacionAcademica';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdFormacionAcademica' => 'int',
		'IdNivelSecundario' => 'int',
		'IdNivelUniversitario' => 'int'
	];

	protected $fillable = [
		'IdNivelSecundario',
		'IdNivelUniversitario'
	];
}
