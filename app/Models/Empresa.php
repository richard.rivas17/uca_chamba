<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Empresa
 * 
 * @property int $IdEmpresa
 * @property string|null $Ruc
 * @property string|null $RazonSocial
 * @property string|null $NombreFantasia
 * @property string|null $UrlEmpresa
 * @property string|null $UrlLogo
 * @property int|null $IdTipoEmpresa
 *
 * @package App\Models
 */
class Empresa extends Model
{
	protected $table = 'Empresas';
	protected $primaryKey = 'IdEmpresa';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdEmpresa' => 'int',
		'IdTipoEmpresa' => 'int'
	];

	protected $fillable = [
		'Ruc',
		'RazonSocial',
		'NombreFantasia',
		'UrlEmpresa',
		'UrlLogo',
		'IdTipoEmpresa'
	];
}
