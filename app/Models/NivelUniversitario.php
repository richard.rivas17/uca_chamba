<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NivelUniversitario
 * 
 * @property int $IdUniversitario
 * @property string|null $Estado
 * @property Carbon|null $AnhoCulminado
 * @property Carbon $FechaAlta
 * @property int|null $IdCarrera
 * @property int|null $IdUniversidad
 * @property int|null $IdSemestreCursando
 *
 * @package App\Models
 */
class NivelUniversitario extends Model
{
	protected $table = 'NivelUniversitario';
	protected $primaryKey = 'IdUniversitario';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdUniversitario' => 'int',
		'AnhoCulminado' => 'datetime',
		'FechaAlta' => 'datetime',
		'IdCarrera' => 'int',
		'IdUniversidad' => 'int',
		'IdSemestreCursando' => 'int'
	];

	protected $fillable = [
		'Estado',
		'AnhoCulminado',
		'FechaAlta',
		'IdCarrera',
		'IdUniversidad',
		'IdSemestreCursando'
	];
}
