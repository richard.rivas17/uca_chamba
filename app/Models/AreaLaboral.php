<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AreaLaboral
 * 
 * @property int $IdAreaLaboral
 * @property string|null $NombreAreaLaboral
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class AreaLaboral extends Model
{
	protected $table = 'AreaLaboral';
	protected $primaryKey = 'IdAreaLaboral';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdAreaLaboral' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'NombreAreaLaboral',
		'FechaAlta'
	];
}
