<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NivelSecundario
 * 
 * @property int $IdSecundario
 * @property string|null $Institucion
 * @property string|null $BachileratoObtenido
 * @property Carbon|null $AnhoCulminado
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class NivelSecundario extends Model
{
	protected $table = 'NivelSecundario';
	protected $primaryKey = 'IdSecundario';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdSecundario' => 'int',
		'AnhoCulminado' => 'datetime',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'Institucion',
		'BachileratoObtenido',
		'AnhoCulminado',
		'FechaAlta'
	];
}
