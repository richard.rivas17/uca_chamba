<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Requerimiento
 * 
 * @property int $IdRequerimiento
 * @property string|null $FormacionAcademica
 * @property int|null $IdPropiedadFormAcademica
 * @property int|null $IdAreaLaboral
 * @property int|null $IdPropiedadAreaLaboral
 * @property int|null $ExperienciaMinima
 * @property int|null $IdPropiedadExperienciaMin
 * @property string|null $HorarioLaboral
 * @property int|null $DisponibilidadViajar
 * @property int|null $IdPropiedadDisponibilidadViajar
 * @property int|null $MovilidadPropia
 * @property int|null $IdPropiedadMovilidadPropia
 * @property Carbon $FechaAlta
 *
 * @package App\Models
 */
class Requerimiento extends Model
{
	protected $table = 'Requerimiento';
	protected $primaryKey = 'IdRequerimiento';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IdRequerimiento' => 'int',
		'IdPropiedadFormAcademica' => 'int',
		'IdAreaLaboral' => 'int',
		'IdPropiedadAreaLaboral' => 'int',
		'ExperienciaMinima' => 'int',
		'IdPropiedadExperienciaMin' => 'int',
		'DisponibilidadViajar' => 'int',
		'IdPropiedadDisponibilidadViajar' => 'int',
		'MovilidadPropia' => 'int',
		'IdPropiedadMovilidadPropia' => 'int',
		'FechaAlta' => 'datetime'
	];

	protected $fillable = [
		'FormacionAcademica',
		'IdPropiedadFormAcademica',
		'IdAreaLaboral',
		'IdPropiedadAreaLaboral',
		'ExperienciaMinima',
		'IdPropiedadExperienciaMin',
		'HorarioLaboral',
		'DisponibilidadViajar',
		'IdPropiedadDisponibilidadViajar',
		'MovilidadPropia',
		'IdPropiedadMovilidadPropia',
		'FechaAlta'
	];
}
