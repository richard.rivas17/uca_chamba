<!-- resources/views/home.blade.php -->
@extends('layouts.app') <!-- Extiende el layout app.blade.php -->

@section('content') <!-- Comienza la sección 'content' -->
<div class="d-flex justify-content-center align-items-center" style="padding-top: 20px;">
    <div style="width: 1000px; max-width: 100%;">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="{{ url('/') }}" class="h1"><b>Nueva Oferta de Empleo</b></a>
            </div>
            <div class="card-body">
                <form action="{{ route('register') }}" method="post">
                    @csrf <!-- Token CSRF para seguridad -->

                    <!-- Título -->
                    <div class="form-group row">
                        <label for="titulo" class="col-sm-3 col-form-label">Título:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="titulo" name="titulo">
                        </div>
                    </div>

                    <!-- Descripción del Cargo -->
                    <div class="form-group row">
                        <label for="descripcion" class="col-sm-3 col-form-label">Descripción del Cargo:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="descripcion" name="descripcion"></textarea>
                        </div>
                    </div>

                    <!-- Ubicación -->
                    <div class="form-group row">
                        <label for="ubicacion" class="col-sm-3 col-form-label">Ubicación:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="ubicacion" name="ubicacion">
                        </div>
                    </div>

                    <!-- Salario Ofertado -->
                    <div class="form-group row">
                        <label for="salario" class="col-sm-3 col-form-label">Salario Ofertado:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="salario" name="salario">
                        </div>
                    </div>

                    <!-- Formación Académica -->
                    <div class="form-group row">
                        <label for="formacion" class="col-sm-3 col-form-label">Formación Académica:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="formacion" name="formacion">
                        </div>
                    </div>

                    <!-- Área Laboral -->
                    <div class="form-group row">
                        <label for="area" class="col-sm-3 col-form-label">Área Laboral:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="area" name="area">
                        </div>
                    </div>

                    <!-- Experiencia Mínima -->
                    <div class="form-group row">
                        <label for="experiencia" class="col-sm-3 col-form-label">Experiencia Mínima:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="experiencia" name="experiencia">
                        </div>
                    </div>

                    <!-- Horario Laboral -->
                    <div class="form-group row">
                        <label for="horario" class="col-sm-3 col-form-label">Horario Laboral:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="horario" name="horario">
                        </div>
                    </div>

                    <!-- Botones de aceptar y cancelar -->
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary btn-block">Aceptar</button>
                        </div>
                        <div class="col-6">
                            <button type="reset" class="btn btn-secondary btn-block">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection <!-- Termina la sección 'content' -->