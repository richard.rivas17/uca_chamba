<!-- resources/views/auth/login.blade.php -->
@extends('layouts.app') <!-- Extiende el layout app.blade.php -->

@section('content') <!-- Comienza la sección 'content' -->
<div class="d-flex justify-content-center align-items-center" style="padding-top: 20px;">
  <div style="width: 600px; max-width: 100%;">
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="{{ url('/') }}" class="h1"><b>Iniciar Sesión</b></a>
      </div>
      <div class="card-body">
        <form action="{{ route('login') }}" method="post">
          @csrf <!-- Token CSRF -->
          <div class="input-group mb-3">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
            </div>
          </div>
        </form>

        <p class="mb-1">
          <a href="{{ route('register') }}">Olvidé mi Contraseña</a>
        </p>
        <p class="mb-0">
          <a href="{{ route('register') }}" class="text-center">Registrarse</a>
        </p>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>

<style>
  .login-box {
    width: 100%;
    max-width: 1000px;
    margin-top: -50px;
  }
</style>
@endsection <!-- Termina la sección 'content' -->