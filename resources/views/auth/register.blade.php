@extends('layouts.app') <!-- Asume que tu layout se llama app.blade.php -->

@section('content')
<div class="d-flex justify-content-center align-items-center" style="padding-top: 20px;">
    <div style="width: 600px; max-width: 100%;">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="{{ url('/') }}" class="h1">Registrarse</a>
            </div>
            <div class="card-body">

                <form action="{{ route('register') }}" method="post">
                    @csrf <!-- Token CSRF para seguridad -->

                    <!-- Checkbox para Postulante o Empleador -->
                    <div class="form-group">
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="postulante" name="tipo_usuario" value="postulante">
                            <label for="postulante">Postulante</label>
                        </div>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="empleador" name="tipo_usuario" value="empleador">
                            <label for="empleador">Empleador</label>
                        </div>
                    </div>
                    <!-- RUC y Razón Social -->
                    <div class="form-group" id="empresaFields" style="display: none;">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="RUC" name="ruc" id="ruc">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Razón Social" name="razon_social" id="razon_social">
                        </div>
                    </div>

                    <!-- Nombre Apellido -->
                    <div class="form-group" id="personalFields">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nombre" name="nombre" id="nombre" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Apellido" name="apellido" id="apellido" required>
                        </div>
                    </div>

                    <!-- Email -->
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" name="email" required>
                    </div>

                    <!-- Repita Email -->
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Repita Email" name="email_confirmation" required>
                    </div>

                    <!-- Contraseña -->
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Contraseña" name="password" required>
                    </div>

                    <!-- Repita Contraseña -->
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Repita Contraseña" name="password_confirmation" required>
                    </div>

                    <!-- Checkbox de términos y condiciones -->
                    <div class="form-group">
                        <div class="icheck-primary">
                            <input type="checkbox" id="terms" name="terms" required>
                            <label for="terms">Acepto los <a href="#">términos y condiciones</a></label>
                        </div>
                    </div>

                    <!-- Botones de aceptar y cancelar -->
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary btn-block">Aceptar</button>
                        </div>
                        <div class="col-6">
                            <button type="reset" class="btn btn-secondary btn-block">Cancelar</button>
                        </div>
                    </div>
                </form>

                <!-- Texto de ya tienes una cuenta -->
                <p class="mt-2">
                    <a href="{{ route('login') }}" class="text-center">Ya tengo una cuenta</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function toggleFields(isEmployer) {
        const showEmpresa = isEmployer;
        document.getElementById('empresaFields').style.display = showEmpresa ? 'block' : 'none';
        document.getElementById('personalFields').style.display = showEmpresa ? 'none' : 'block';

        // Ajustar campos requeridos
        document.getElementById('ruc').required = showEmpresa;
        document.getElementById('razon_social').required = showEmpresa;
        document.getElementById('nombre').required = !showEmpresa;
        document.getElementById('apellido').required = !showEmpresa;
    }

    document.getElementById('postulante').addEventListener('change', function() {
        document.getElementById('empleador').checked = !this.checked;
        toggleFields(!this.checked);
    });

    document.getElementById('empleador').addEventListener('change', function() {
        document.getElementById('postulante').checked = !this.checked;
        toggleFields(this.checked);
    });
</script>
@endsection