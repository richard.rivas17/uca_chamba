<!-- resources/views/home.blade.php -->
@extends('layouts.app') <!-- Extiende el layout app.blade.php -->

@section('content') <!-- Comienza la sección 'content' -->
<div class="d-flex justify-content-center align-items-center" style="padding-top: 20px;">
    <div style="width: 1000px; max-width: 100%;">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="{{ url('/') }}" class="h1">Mi Perfil</a>
            </div>
            <div class="card-body">

                <form action="{{ route('register') }}" method="post">
                    @csrf <!-- Token CSRF para seguridad -->

                    <!-- Nombre -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Nombres:</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>

                    <!-- Apellido -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Apellidos:</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>

                    <!-- Email -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Email:</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>

                    <!-- Nacionalidad -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Nacionalidad:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    <!-- Tipo de Documento -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Tipo de Documento:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    <!-- Nro. de Documento -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Nro. de Documento:</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>

                    <!-- Edad -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Edad:</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>

                    <!-- Estado Civil -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Estado Civil:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    <!-- Genero -->
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Genero:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    <!-- Botones de aceptar y cancelar -->
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary btn-block">Aceptar</button>
                        </div>
                        <div class="col-6">
                            <button type="reset" class="btn btn-secondary btn-block">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection <!-- Termina la sección 'content' -->