<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Aplicación Laravel</title>
    <!-- @vite(['resources/css/app.css', 'resources/js/app.js']) -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">
    <style>
        .main-footer {
            background-color: #0E4AA2;
            /* Un tono de azul, puedes cambiarlo por el color exacto que necesites */
            color: white;
            /* Cambia el color del texto a blanco para mejor contraste, si es necesario */
        }

        .main-footer .img-fluid {
            margin: 20px;
            /* Ajusta el valor según tus necesidades */
            max-width: 70px;
            /* Ajusta esto según tus necesidades */
            height: auto;
        }
    </style>
</head>

<body class="hold-transition layout-top-nav"">

<div class=" wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
            <a href="" class="navbar-brand">
                <img src="{{ asset('img/logo-uc.png') }}" alt="Descripción" style="height: 122px; width: 212px;">
            </a>

            <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                <li class="nav-item">
                    <a href="index3.html" class="nav-link">Acerca del Centro</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Contacto</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <img src="{{ asset('img/logo-simple.png') }}" alt="Imagen Central" class="img-fluid">
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="col-auto">
                    <img src="{{ asset('img/facebook.png') }}" alt="Imagen 1" class="img-fluid">
                </div>
                <div class="col-auto">
                    <img src="{{ asset('img/twitter.png') }}" alt="Imagen 2" class="img-fluid">
                </div>
                <div class="col-auto">
                    <img src="{{ asset('img/instagram.png') }}" alt="Imagen 3" class="img-fluid">
                </div>
                <div class="col-auto">
                    <img src="{{ asset('img/youtube.png') }}" alt="Imagen 4" class="img-fluid">
                </div>
            </div>
        </div>
    </footer>


    <!-- Control Sidebar (optional) -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Aquí puedes colocar contenido adicional -->
    </aside>
    <!-- /.control-sidebar -->

    </div>
    <!-- ./wrapper -->

    @yield('scripts')
</body>

</html>