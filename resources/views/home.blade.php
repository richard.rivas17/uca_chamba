<!-- resources/views/home.blade.php -->
@extends('layouts.app') <!-- Extiende el layout app.blade.php -->

@section('content') <!-- Comienza la sección 'content' -->
<div class="container">
    <div class="row">
        <!-- Primera fila -->
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h3>Centro de Empleos</h3>
                    <p>Creando Nueva Oportunidades</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Usuario -->
                    <div class="mb-3">
                        <label for="usuario" class="form-label">Usuario</label>
                        <input type="text" class="form-control" id="usuario">
                    </div>

                    <!-- Contraseña -->
                    <div class="mb-3">
                        <label for="password" class="form-label">Contraseña</label>
                        <input type="password" class="form-control" id="password">
                    </div>

                    <!-- Botón Iniciar Sesión -->
                    <button class="btn btn-primary d-block w-100" type="button">Iniciar Sesión</button>

                    <!-- Texto 'o' -->
                    <div class="text-center my-2">
                        o
                    </div>

                    <!-- Botón Crear Cuenta -->
                    <button class="btn btn-secondary d-block w-100" type="button">Crear Cuenta</button>

                    <!-- Olvidaste tu contraseña -->
                    <div class="text-center mt-3">
                        <a href="#">¿Olvidaste tu contraseña?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Segunda fila -->
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4>¿Qué es el centro de empleos?</h4>
                    <p>Somos un centro de apoyo orientado a vincular personas que buscan un crecimiento profesional a través del primer empleo o nuevas oportunidades laborales. Creyendo firmemente que la base de toda empresa es el talento humano. Tenemos el compromiso de brindar los mejores talentos a nuestras empresas asociadas. alineados a sus necesidades y cultura organizacional.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4>¿Con quiénes trabajamos?</h4>
                    <p>Con empresas referentes del mercado, buscamos seleccionar los mejores talentos y posicionarlos de acuerdo con sus aptitudes, capacidades y competencias, que les permitirá alcanzar su máximo potencial y desarrollar sus talentos</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection <!-- Termina la sección 'content' -->
